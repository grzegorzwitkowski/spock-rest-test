package spocktest

import spock.lang.Specification
import spock.lang.Unroll

import static UserTestBuilder.userWith

class SpockFeaturesTest extends Specification {

    @Unroll
    def "data driven test #a + #b == #exp"() {
        expect:
        a + b == exp

        where:
        a | b || exp
        1 | 2 || 3
        4 | 4 || 8
    }

    def "field extraction"() {
        given:
        List<User> users = [userWith(id: "1"), userWith(id: "2")]

        expect:
        users.id == ["1", "2"]
    }
}
