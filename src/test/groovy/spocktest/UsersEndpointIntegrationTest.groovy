package spocktest

import groovyx.net.http.RESTClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.embedded.LocalServerPort
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import static UserTestBuilder.userWith

@ContextConfiguration
@SpringBootTest(webEnvironment = RANDOM_PORT)
class UsersEndpointIntegrationTest extends Specification {

    @LocalServerPort
    int port

    RESTClient restClient

    @Autowired
    TestRestTemplate restTemplate

    void setup() {
        restClient = new RESTClient("http://localhost:$port")
        restClient.handler.failure = restClient.handler.success
    }

    def "should get user data"() {
        given:
        User userToCreate = userWith(id: "2", firstName: "Anna", lastName: "Nowak", email: "anna.nowak@email.com")
        restTemplate.postForObject("/users", userToCreate, Void)

        when:
        def response = restClient.get([path: "/users/2"])

        then:
        with(response.responseData) {
            firstName == "Anna"
            lastName == "Nowak"
            email == "anna.nowak@email.com"
        }
    }

    def "should return 404 when user was not found"() {
        when:
        def response = restClient.get([path: "/users/3"])

        then:
        response.status == 404
    }
}
