package spocktest

class UserTestBuilder {

    static User userWith(Map params) {
        new User(
                params.id ?: "1001",
                params.firstName ?: "Jan",
                params.lastName ?: "Kowalski",
                params.email ?: "jan.kowalski@fakemail.com"
        )
    }
}
