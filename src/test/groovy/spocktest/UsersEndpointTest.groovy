package spocktest

import spock.lang.Specification

import static UserTestBuilder.userWith

class UsersEndpointTest extends Specification {

    UsersRepository usersRepository = Mock()
    UsersEndpoint usersEndpoint = new UsersEndpoint(usersRepository)

    def "should throw exception when user was not found"() {
        given:
        usersRepository.getById("1") >> { throw new UserNotFoundException("1") }

        when:
        usersEndpoint.getById("1")

        then:
        thrown UserNotFoundException
    }

    def "should return user data"() {
        when:
        User u = usersEndpoint.getById("1")

        then:
        u.id == "1"
        1 * usersRepository.getById("1") >> Optional.of(userWith(id: "1"))
    }
}
