package spocktest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class UsersEndpoint {

    private UsersRepository usersRepository;

    @Autowired
    public UsersEndpoint(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @RequestMapping(path = "/users/{userId}", method = GET, produces = APPLICATION_JSON_VALUE)
    public User getById(@PathVariable("userId") String userId) {
        return usersRepository.getById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));
    }

    @RequestMapping(path = "/users", method = POST, consumes = APPLICATION_JSON_VALUE)
    public void save(@RequestBody User user) {
        usersRepository.save(user);
    }
}
